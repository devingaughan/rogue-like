﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour


{

    public static GameController Instance;
    public bool isPlayerTurn;
    public bool areEnemiesMoving;
    public int playerCurrentHealth = 50;
    public AudioClip gameOverSound;
    public GameObject easy;
    public GameObject hard;


    private BoardController boardController;
    private List<Enemy> enemies;
    private GameObject menu;
    private Text menuText;
    private GameObject characterSelect;
    private GameObject levelImage;
    private Text levelText;
    private bool settingUpGame;
    private int secondsUntilLevelStart = 2;
    private int currentLevel = 1;
    private int score;
    public int difficulty = 2; //1 means easy, 2 means average, 3 means hard
  
    public void Easy()
    {
        easy.SetActive(true);
    }

    public void Hard()
    {
        hard.SetActive(true);
    }

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            DestroyImmediate(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        boardController = GetComponent<BoardController>();
        enemies = new List<Enemy>();
    }

    void Start()
    {
        IntitializeGame();
        menu = GameObject.Find("menu");
        menuText = GameObject.Find("menuText").GetComponent<Text>();
        score = 0;


    }

      

    private void IntitializeGame()
    {
        Debug.Log(currentLevel.ToString());
        levelImage = GameObject.Find("Level Image");
        levelText = GameObject.Find("Level Text").GetComponent<Text>();
   if(currentLevel > 1)
        {
            levelImage.SetActive(true);
            levelText.text = "Day " + currentLevel;
            menu = GameObject.Find("menu");
            menu.SetActive(false);
        }

     

        enemies.Clear();
        settingUpGame = false;

        boardController.SetupLevel(currentLevel);
        Invoke("DisableLevelImage", secondsUntilLevelStart);

        settingUpGame = true;
    }

    public void potion()
    {
       
    }


    public void Disablemenu()
    {
        menu.SetActive(false);
        
    }

    public void disableAverage()
    {
        menu.SetActive(false);
        
        if(difficulty == 2)
        {
            Instance.disableAverage();        
        } 
        
         
              
        
        
    }

    public void disableInsane()
    {
        menu.SetActive(false);
        

    }
    private void DisableLevelImage()
    {
        levelImage.SetActive(false);
        settingUpGame = false;
        isPlayerTurn = true;
        areEnemiesMoving = false;
    }


    private void OnLevelWasLoaded(int levelLoaded)
    {
        currentLevel++;
        IntitializeGame();
    }


    void Update()
    {
        if (isPlayerTurn || areEnemiesMoving || settingUpGame)
        {
            return;
        }

        StartCoroutine(MoveEnemies());
    }

    private IEnumerator MoveEnemies()
    {
        areEnemiesMoving = true;

        yield return new WaitForSeconds(0.2f);

        foreach (Enemy enemy in enemies)
        {
            enemy.MoveEnemy();
            yield return new WaitForSeconds(enemy.moveTime);
        }

        areEnemiesMoving = false;
        isPlayerTurn = true;
    }

    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }
     private bool CheckHighscore()
    {
        if (currentLevel > PlayerPrefs.GetInt("highscore", 0))
        {
            return true;
        }
        else return false;
    }
    private int GetHighscore()
    {
    if( CheckHighscore() )
        {
            PlayerPrefs.SetInt("highscore", currentLevel);
        }
        return PlayerPrefs.GetInt("highscore", 0);
    }
    public void GameOver()
    {
        bool brokeHighscore = CheckHighscore();
        int highscore = GetHighscore();
        isPlayerTurn = false;
        SoundController.Instance.music.Stop();
        SoundController.Instance.PlaySingle(gameOverSound);
        if(brokeHighscore)
        {
            levelText.text = "you starved after " + currentLevel + " days...\n" + "Congratulations New Highscore: " + highscore;
        }
        else
        {
            levelText.text = "you starved after " + currentLevel + " days...\n" + "Old Highscore: " + highscore;
        }
        levelImage.SetActive(true);
        enabled = false;
    }
    
            }


